%{
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
void yyerror(const char *message);
int i = 0, count = 0, parCount = 0;
int yydebug=1;
bool redefine = false;

char funTable [100][100] = {'\0'};
int repeatVar [100];

char varTable [100][100] = {'\0'};
int tableSize = 0;
%}
%code requires {
#include <stdbool.h>
    struct expr{
        enum { INTEGER, BOOL } type;
        union{
            int   ival;
            bool bval;
        } value;
    };
    struct expr valueTable [100];
    struct expr tmpTable [100];
    struct expr params [100];
}
%union{
    struct expr nval;
    struct expr par [100];
    char* word;
}
%define parse.error verbose
%token<nval> NUMBER BOOLEAN
%token<word> ID
%token LPAR RPAR PRINT_NUM PRINT_BOOL PLUS MINUS MUL DIV MOD GT LT EQ SEP AND OR NOT DEF FUN IF

%type<nval> num_op logical_op exp plusExps mulExps eqExps andExps orExps
%type<nval> plus minus multiply divide modulus greater smaller equal
%type<nval> and_op or_op not_op
%type<nval> if_exp test_exp then_exp else_exp
%type<word> variable
/* %type<word>
%left PLUS MINUS
%left MUL DIV MOD */

%%

program : stmts ;

stmts: stmt stmts
    | ;
    ;
stmt: exp SEP | def_stmt SEP | print_stmt SEP;
print_stmt: LPAR PRINT_NUM SEP exp RPAR {
        if($4.type == BOOL){
            puts("Type Error: expect 'number' but got 'boolean'.");
            return 0;
        }
        printf("%d\n", $4.value.ival);
    }
    | LPAR PRINT_BOOL SEP exp RPAR {
        if($4.type == INTEGER){
            puts("Type Error: expect 'boolean' but got 'number'.");
            return 0;
        }
        if($4.value.bval == true){
            puts("#t");
        }
        else{
            puts("#f");
        }
    }
    ;
exp: BOOLEAN {
        $$.type = $1.type;
        $$.value.bval = $1.value.bval;
    }
    | NUMBER {
        $$.type = $1.type;
        $$.value.ival = $1.value.ival;
    }
    | variable {
        i = 0;
        while(i != tableSize){
            if(!strcmp(varTable[i], $1)){
                $$.type = valueTable[i].type;
                if($$.type == BOOL){
                    $$.value.bval = valueTable[i].value.bval;
                }
                else{
                    $$.value.ival = valueTable[i].value.ival;
                }
                break;
            }
            i++;
        }
    }
    | num_op {
        $$.type = $1.type;
        $$.value.ival = $1.value.ival;
    }
    | logical_op {
        if($1.type == INTEGER){
            puts("Type Error: expect 'boolean' but got 'number'.");
            return 0;
        }
        $$.type = $1.type;
        $$.value.bval = $1.value.bval;
    }
    /* | fun_exp | fun_call  */
    | if_exp{
        $$.type = $1.type;
        if($1.type == BOOL){
            $$.value.bval = $1.value.bval;
        }
        else{
            $$.value.ival = $1.value.ival;
        }
    }
;



num_op: plus {
        $$.type = $1.type;
        $$.value.ival = $1.value.ival;
    }
    | minus  {
        $$.type = $1.type;
        $$.value.ival = $1.value.ival;
    }
    | multiply  {
        $$.type = $1.type;
        $$.value.ival = $1.value.ival;
    }
    | divide {
        $$.type = $1.type;
        $$.value.ival = $1.value.ival;
    }
    | modulus {
        $$.type = $1.type;
        $$.value.ival = $1.value.ival;
    }
    | greater {
        $$.type = $1.type;
        $$.value.bval = $1.value.bval;
    }
    | smaller {
        $$.type = $1.type;
        $$.value.bval = $1.value.bval;
    }
    | equal {
        $$.type = $1.type;
        $$.value.bval = $1.value.bval;
    }
    ;
plus: LPAR PLUS SEP plusExps RPAR {
    $$.type = $4.type;
    $$.value.ival = $4.value.ival;
};
plusExps: exp SEP plusExps{
        if($1.type == BOOL){
            puts("Type Error: expect 'number' but got 'boolean'.");
            return 0;
        }
        $$.type = $1.type;
        $$.value.ival = $1.value.ival + $3.value.ival;
    }
    | exp {
        if($1.type == BOOL){
            puts("Type Error: expect 'number' but got 'boolean'.");
            return 0;
        }
        $$.type = $1.type;
        $$.value.ival = $1.value.ival;
    };
minus: LPAR MINUS SEP exp SEP exp RPAR {
    if($4.type == BOOL || $6.type == BOOL){
        puts("Type Error: expect 'number' but got 'boolean'.");
        return 0;
    }
    $$.type = $4.type;
    $$.value.ival = $4.value.ival - $6.value.ival;
};
multiply: LPAR MUL SEP mulExps RPAR {
    $$.type = $4.type;
    $$.value.ival = $4.value.ival;
};
mulExps: exp SEP mulExps{
        if($1.type == BOOL){
            puts("Type Error: expect 'number' but got 'boolean'.");
            return 0;
        }
        $$.type = $1.type;
        $$.value.ival = $1.value.ival*$3.value.ival;
    }
    | exp {
        if($1.type == BOOL){
            puts("Type Error: expect 'number' but got 'boolean'.");
            return 0;
        }
        $$.type = $1.type;
        $$.value.ival = $1.value.ival;
    };
divide: LPAR DIV SEP exp SEP exp RPAR {
    if($4.type == BOOL || $6.type == BOOL){
        puts("Type Error: expect 'number' but got 'boolean'.");
        return 0;
    }
    $$.type = $4.type;
    $$.value.ival = $4.value.ival / $6.value.ival;
};
modulus: LPAR MOD SEP exp SEP exp RPAR {
    if($4.type == BOOL || $6.type == BOOL){
        puts("Type Error: expect 'number' but got 'boolean'.");
        return 0;
    }
    $$.type = $4.type;
    $$.value.ival = $4.value.ival % $6.value.ival;
};
greater: LPAR GT SEP exp SEP exp RPAR {
    if($4.type == BOOL || $6.type == BOOL){
        puts("Type Error: expect 'number' but got 'boolean'.");
        return 0;
    }
    $$.type = BOOL;
    if($4.value.ival > $6.value.ival){
        $$.value.bval = true;
    }
    else{
        $$.value.bval = false;
    }
};
smaller: LPAR LT SEP exp SEP exp RPAR {
    if($4.type == BOOL || $6.type == BOOL){
        puts("Type Error: expect 'number' but got 'boolean'.");
        return 0;
    }
    $$.type = BOOL;
    if($4.value.ival < $6.value.ival){
        $$.value.bval = true;
    }
    else{
        $$.value.bval = false;
    }
};
equal: LPAR EQ SEP eqExps RPAR  {
    $$.type = $4.type;
    $$.value.bval = $4.value.bval;
};
eqExps: exp SEP eqExps{
    if($1.type == BOOL || $3.type == BOOL){
        puts("Type Error: expect 'number' but got 'boolean'.");
        return 0;
    }
        $$.type = BOOL;
        if($1.value.ival == $3.value.ival){
            $$.value.bval = true;
        }
        else{
            $$.value.bval = false;
        }
    }
    | exp {
        if($1.type == BOOL){
            puts("Type Error: expect 'number' but got 'boolean'.");
            return 0;
        }
        $$.type = $1.type;
        $$.value.ival = $1.value.ival;
    };


logical_op: and_op {
        $$.type = $1.type;
        $$.value.bval = $1.value.bval;
    }
    | or_op  {
        $$.type = $1.type;
        $$.value.bval = $1.value.bval;
    }
    | not_op {
        $$.type = $1.type;
        $$.value.bval = $1.value.bval;
    }
    ;
and_op: LPAR AND SEP andExps RPAR {
    $$.type = $4.type;
    $$.value.bval = $4.value.bval;
};
andExps: exp SEP andExps{
        if($1.type == INTEGER || $3.type == INTEGER){
            puts("Type Error: expect 'boolean' but got 'number'.");
            return 0;
        }
        $$.type = BOOL;
        $$.value.bval = ($1.value.bval && $3.value.bval);
    }
    | exp {
        if($1.type == INTEGER){
            puts("Type Error: expect 'boolean' but got 'number'.");
            return 0;
        }
        $$.type = BOOL;
        $$.value.bval = $1.value.bval;
    };
or_op: LPAR OR SEP orExps RPAR {
        $$.type = $4.type;
        $$.value.bval = $4.value.bval;
    };
orExps: exp SEP orExps{
        if($1.type == INTEGER || $3.type == INTEGER){
            puts("Type Error: expect 'boolean' but got 'number'.");
            return 0;
        }
        $$.type = BOOL;
        $$.value.bval = ($1.value.bval || $3.value.bval);
    }
    | exp {
        if($1.type == INTEGER){
            puts("Type Error: expect 'boolean' but got 'number'.");
            return 0;
        }
        $$.type = BOOL;
        $$.value.bval = $1.value.bval;
    };
not_op: LPAR NOT SEP exp RPAR {
        if($4.type == INTEGER){
            puts("Type Error: expect 'boolean' but got 'number'.");
            return 0;
        }
        $$.type = BOOL;
        $$.value.bval = (!$4.value.bval);
    };



def_stmt: LPAR DEF SEP variable SEP exp RPAR{
    i = 0;
    redefine = false;
    while(i != tableSize){
        if(!strcmp(varTable[i++], $4)){
            puts("redefine variable.");
            redefine = true;
            break;
        }
    }
    if(!redefine){
        strcpy(varTable[tableSize], $4);
        valueTable[tableSize].type = $6.type;
        if($6.type == BOOL){
            valueTable[tableSize++].value.bval = $6.value.bval;
        }
        else{
            valueTable[tableSize++].value.ival = $6.value.ival;
        }
    }
};
variable: ID {
    strcpy($$, $1);
};


/* fun_exp: LPAR FUN SEP LPAR fun_ids RPAR SEP fun_body RPAR;
fun_ids: ID fun_ids {
    i = 0, count = 0;
    redefine = false;
    while(i != tableSize){
        if(!strcmp(varTable[i++], $1)){
            redefine = true;
            repeatVar[count] = i;
            count++;
        }
    }
    if(!redefine){
        strcpy(varTable[tableSize], $1);
        valueTable[tableSize].type = $1.type;
        if($1.type == BOOL){
            valueTable[tableSize++].value.bval = $1.value.bval;
        }
        else{
            valueTable[tableSize++].value.ival = $1.value.ival;
        }
    }
    else{
        while(i != count){
            tmpTable[i].type = valueTable[repeatVar[i]].type;
            if(tmpTable[i].type == BOOL){
                tmpTable[i].value.bval = valueTable[repeatVar[i]].value.bval;
            }
            else{
                tmpTable[i].value.ival = valueTable[repeatVar[i]].value.ival;
            }

            valueTable[repeatVar[i]] = $-2[i].type;
            if($-2[i].type == BOOL){
                valueTable[repeatVar[i]].value.bval = $-2[i].value.bval;
            }
            else{
                valueTable[repeatVar[i]].value.ival = $-2[i].value.ival;
            }
        }
    }
}
| ;;
fun_body: exp {
    $$.type = $1.type;
    if($$.type == BOOL){
        $$.value.bval = $1.value.bval;
    }
    else{
        $$.value.ival = $1.value.ival;
    }
};
fun_call: LPAR tmp{
    $2 = $5;
    
} fun_exp param RPAR {
    
}
/* | LPAR fun_name param RPAR{

}   
;*/
/* params: { arcount = 0;  }param{ $$ = params;};


// the sep is in param
param: SEP exp param {
    params[parcount].type = $2.type;
    if(params[parcount].type == BOOL){
        params[parcount].value.bval = $2.value.bval;
    }
    else{
        params[parcount].value.ival = $2.value.ival;
    }
    printf("%d  %d  %d  %d",parcount, params[parcount].type, params[parcount].value.bval, params[parcount].value.ival);
    parcount++;
    
}| ;;
last_exp: exp {
    $$.type = $1.type;
    if($$.type == BOOL){
        $$.value.bval = $1.value.bval;
    }
    else{
        $$.value.ival = $1.value.ival;
    }
};
fun_name: ID; */


if_exp: LPAR IF SEP test_exp SEP then_exp SEP else_exp RPAR{
    if($4.value.bval == true){
        $$.type = $6.type;
        if($6.type == BOOL){
            $$.value.bval = $6.value.bval;
        }
        else{
            $$.value.ival = $6.value.ival;
        }
    }
    else{
        $$.type = $8.type;
        if($8.type == BOOL){
            $$.value.bval = $8.value.bval;
        }
        else{
            $$.value.ival = $8.value.ival;
        }
    }
};
test_exp: exp {
    if($1.type == INTEGER){
        puts("Type Error: expect 'boolean' but got 'number'.");
        return 0;
    }
    $$.type = BOOL;
    $$.value.bval = $1.value.bval;
};
then_exp: exp {
        $$.type = $1.type;
        if($1.type == BOOL){
            $$.value.bval = $1.value.bval;
        }
        else{
            $$.value.ival = $1.value.ival;
        }
    };
else_exp: exp {
        $$.type = $1.type;
        if($1.type == BOOL){
            $$.value.bval = $1.value.bval;
        }
        else{
            $$.value.ival = $1.value.ival;
        }
    };

%%

int main()
{
    yyparse();
    return 0;
}

void yyerror(const char *message)
{	
	puts("syntax error");
}