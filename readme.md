# compiler final project

## 已實現的功能
- Syntax Validation
- Print
- Numerical Operations
- Logical Operations
- if Expression 
- Variable Definition

## 編譯
```shell=
bison -d -o final.tab.c final.y
gcc -c -g -I.. final.tab.c
flex -o lex.yy.c final.l
gcc -c -g -I.. lex.yy.c
gcc -o final final.tab.o lex.yy.o -ll
```

## 測試方式
```shell=
cat input.txt | ./final
```
