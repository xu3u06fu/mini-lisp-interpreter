%{
#include "final.tab.h"
#include <stdio.h>
#include <stdbool.h>
int col = 0;
%}
separator  [ \t\n\f\r]+
letter  [a-z]
digit  [0-9]
boolean  (#t|#f)
number  (0|[1-9]{digit}*|\-[1-9]{digit}*)
id  {letter}({letter}|{digit}|'-')*


%%
"+"   {
    return PLUS;
}
"-"   {
    return MINUS;
}
"*"   {
    return MUL;
}
"/"   {
    return DIV;
}
"mod"   {
    return MOD;
}
">"   {
    return GT;
}
"<"   {
    return LT;
}
"="   {
    return EQ;
}
"("   {
    return LPAR;
}
")"   {
    return RPAR;
}
"and"   {
    return AND;
}
"or"   {
    return OR;
}
"not"   {
    return NOT;
}
"define"   {
    return DEF;
}
"lambda"   {
    return FUN;
}
"if"   {
    return IF;
}
"print-num"  {
    return PRINT_NUM;
}
"print-bool"  {
    return PRINT_BOOL;
}
{id}   {
    yylval.word = strdup(yytext);
    return ID;
}
{boolean}  {
    yylval.nval.type = BOOL;
    yylval.nval.value.bval = (yytext[1] == 't' ? true : false);
    return BOOLEAN;
}
{separator}   {
    return SEP;
}
{number}   {
    yylval.nval.type = INTEGER;
    yylval.nval.value.ival = atoi(yytext);
    return NUMBER;
}

%%